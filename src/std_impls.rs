use std::collections::{BTreeMap, HashMap};
use std::io;

use crate::{PPrint, PrettyPrinter, INDENT};

macro_rules! as_debug {
    ($($t:ty),+) => {
        $(
            impl PPrint for $t { }
        )+
    };
}

as_debug! {
    String, &str, bool, (),
    i8, i16, i32, i64, i128, isize,
    u8, u16, u32, u64, u128, usize,
    f32, f64
}

// TODO: Tuples, and the rest for std::collections
// TODO: My own structs (derive)

trait Len {
    fn len(&self) -> usize;
}

impl<K, V, S> Len for HashMap<K, V, S> {
    fn len(&self) -> usize {
        self.len()
    }
}

impl<K, V> Len for BTreeMap<K, V> {
    fn len(&self) -> usize {
        self.len()
    }
}

fn dispatch_map<K, V, M>(
    object: &M,
    stream: &mut dyn io::Write,
    pprint: &PrettyPrinter,
    mut indent: usize,
) -> io::Result<()>
where
    for<'a> &'a M: IntoIterator<Item = (&'a K, &'a V)>,
    K: PPrint,
    V: PPrint,
    M: Len,
{
    indent += INDENT;
    write!(stream, "{{")?;
    let delimnl: String = String::from(",\n") + &" ".repeat(indent);
    let last_index = object.len() - 1;

    for (i, (k, v)) in object.into_iter().enumerate() {
        let last = i == last_index;
        let krep = format!("{:?}", k);
        write!(stream, "{}: ", krep)?;
        pprint.format(v, stream, indent + krep.len() + 2)?;
        if !last {
            write!(stream, "{}", delimnl)?;
        }
    }

    write!(stream, "}}")
}

fn dispatch_option<T: PPrint>(
    object: &Option<T>,
    stream: &mut dyn io::Write,
    pprint: &PrettyPrinter,
    mut indent: usize,
) -> io::Result<()> {
    if let Some(inner) = object {
        write!(stream, "Some(")?;
        indent += INDENT;
        pprint.format(inner, stream, indent)?;
        write!(stream, ")")
    } else {
        write!(stream, "None")
    }
}

fn dispatch_slice<T: PPrint>(
    object: &[T],
    stream: &mut dyn io::Write,
    pprint: &PrettyPrinter,
    mut indent: usize,
) -> io::Result<()> {
    write!(stream, "[")?;

    indent += INDENT;

    let delimnl: String = String::from(",\n") + &" ".repeat(indent);
    let mut delim = "";

    let mut last = false;
    let mut iter = object.iter();

    let mut next_ent = match iter.next() {
        Some(x) => x,
        None => return Ok(()),
    };

    while !last {
        let ent = next_ent;
        match iter.next() {
            Some(x) => next_ent = x,
            None => {
                last = true;
            }
        }
        write!(stream, "{}", delim)?;
        delim = &delimnl;
        pprint.format(ent, stream, indent)?;
    }

    write!(stream, "]")
}

impl<T: PPrint> PPrint for Vec<T> {
    fn dispatch() -> Option<crate::Dispatch<Self>> {
        Some(|object, stream, pprint, indent| dispatch_slice(object, stream, pprint, indent))
    }
}

impl<T: PPrint> PPrint for &[T] {
    fn dispatch() -> Option<crate::Dispatch<Self>> {
        Some(|object, stream, pprint, indent| dispatch_slice(object, stream, pprint, indent))
    }
}

impl<T: PPrint, const N: usize> PPrint for [T; N] {
    fn dispatch() -> Option<crate::Dispatch<Self>> {
        Some(|object, stream, pprint, indent| dispatch_slice(object, stream, pprint, indent))
    }
}

impl<K: PPrint + Ord, V: PPrint, S> PPrint for HashMap<K, V, S> {
    fn dispatch() -> Option<crate::Dispatch<Self>> {
        Some(dispatch_map)
    }
}

impl<K: PPrint, V: PPrint> PPrint for BTreeMap<K, V> {
    fn dispatch() -> Option<crate::Dispatch<Self>> {
        Some(dispatch_map)
    }
}

impl<T: PPrint> PPrint for Option<T> {
    fn dispatch() -> Option<crate::Dispatch<Self>> {
        Some(dispatch_option)
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::pprint;
    use insta::assert_snapshot;

    #[test]
    fn vec() {
        let v = vec![
            vec![1, 2, 3452, 34352435, 234534, 3425435],
            vec![213415, 4352345, 2435234, 243543],
            vec![32412, 1234123, 3154],
        ];

        let out = pprint(&v);
        assert_snapshot!(out);
    }

    #[test]
    fn hashmap() {
        use literally::bmap;

        let m: BTreeMap<&str, BTreeMap<&str, &str>> = bmap! {
            "foo" => bmap!{
                "bar"=> "baz"
            },
            "bar" => bmap!{},
            "baz" => bmap!{
                "a"=> "b",
                "c"=> "d",
                "e"=> "f",
                "g"=> "h"
            }
        };

        assert_snapshot!(pprint(&m));
    }

    #[test]
    fn nested_vec() {
        let x = vec![vec![vec![10; 10]; 10]; 10];

        assert_snapshot!(pprint(&x));
    }

    #[test]
    fn very_nested() {
        let x = vec![vec![vec![vec![vec![vec![2; 2]; 2]; 2]; 2]; 2]; 2];
        assert_snapshot!(pprint(&x));
    }

    #[test]
    fn options() {
        let a: [Option<&[u64]>; 6] = [
            Some(&[1, 2, 4532, 25243, 341231]),
            None,
            None,
            Some(&[
                2414, 12341234, 1341234, 12341234, 123432413, 1324134, 1341324, 12341324, 1324431,
                12341234,
            ]),
            Some(&[]),
            Some(&[1, 2]),
        ];
        assert_snapshot!(pprint(&a));
    }

    #[test]
    fn readme_demo() {
        let complex_structure = vec![
            vec![Some(1), Some(2), Some(3), None],
            vec![Some(2), None],
            vec![Some(4), Some(7)],
            vec![Some(1), Some(2), Some(3), None],
            vec![Some(2), None],
            vec![Some(4), Some(7)],
            vec![Some(1), Some(2), Some(3), None],
            vec![Some(2), None],
            vec![Some(4), Some(7)],
        ];
        assert_snapshot!(pprint(&complex_structure))
    }
}
