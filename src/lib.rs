//! `pprint` is a pritty printing crate for rust.
//!
//! It is based on [`std::fmt`] and python's [`pprint`](https://docs.python.org/3/library/pprint.html)
//!
//! # Why not just use [`Debug`]
//!
//! The [`Debug`] trait is good, but the problem is it is not very good at nested stuctures.
//! Either you use `{:?}` and get a line that is too long, or a to many lines with not enough
//! information on them.
//!
//! ```rust
//! let complex_structure = vec![
//!     vec![Some(1), Some(2), Some(3), None],
//!     vec![Some(2), None],
//!     vec![Some(4), Some(7)],
//!     vec![Some(1), Some(2), Some(3), None],
//!     vec![Some(2), None],
//!     vec![Some(4), Some(7)],
//!     vec![Some(1), Some(2), Some(3), None],
//!     vec![Some(2), None],
//!     vec![Some(4), Some(7)],
//! ];
//!
//! let one_line = format!("{:?}", complex_structure);
//!
//! assert_eq!(one_line, "[[Some(1), Some(2), Some(3), None], [Some(2), None], [Some(4), Some(7)], [Some(1), Some(2), Some(3), None], [Some(2), None], [Some(4), Some(7)], [Some(1), Some(2), Some(3), None], [Some(2), None], [Some(4), Some(7)]]");
//!
//! let many_lines = format!("{:#?}", complex_structure);
//!
//! assert_eq!(many_lines, "[
//!     [
//!         Some(
//!             1,
//!         ),
//!         Some(
//!             2,
//!         ),
//!         Some(
//!             3,
//!         ),
//!         None,
//!     ],
//!     [
//!         Some(
//!             2,
//!         ),
//!         None,
//!     ],
//!     [
//!         Some(
//!             4,
//!         ),
//!         Some(
//!             7,
//!         ),
//!     ],
//!     [
//!         Some(
//!             1,
//!         ),
//!         Some(
//!             2,
//!         ),
//!         Some(
//!             3,
//!         ),
//!         None,
//!     ],
//!     [
//!         Some(
//!             2,
//!         ),
//!         None,
//!     ],
//!     [
//!         Some(
//!             4,
//!         ),
//!         Some(
//!             7,
//!         ),
//!     ],
//!     [
//!         Some(
//!             1,
//!         ),
//!         Some(
//!             2,
//!         ),
//!         Some(
//!             3,
//!         ),
//!         None,
//!     ],
//!     [
//!         Some(
//!             2,
//!         ),
//!         None,
//!     ],
//!     [
//!         Some(
//!             4,
//!         ),
//!         Some(
//!             7,
//!         ),
//!     ],
//! ]")
//! ```
//!
//! `pprint` aims to be a third alternative, that gives a resonable comprimise
//!
//! ```text
//! [
//!     [Some(1), Some(2), Some(3), None],
//!     [Some(2), None],
//!     [Some(4), Some(7)],
//!     [Some(1), Some(2), Some(3), None],
//!     [Some(2), None],
//!     [Some(4), Some(7)],
//!     [Some(1), Some(2), Some(3), None],
//!     [Some(2), None],
//!     [Some(4), Some(7)],
//! ]
//! ```
//!
//!

use std::fmt::Debug;
use std::{io, usize};

mod std_impls;

const INDENT: usize = 1;

type Dispatch<T> = for<'w> fn(
    object: &T,
    stream: &'w mut dyn io::Write,
    pprint: &PrettyPrinter,
    indent: usize,
) -> io::Result<()>;

pub fn pprint<T: PPrint>(obj: &T) -> String {
    let pp = PrettyPrinter { width: 80 };
    let mut out = Vec::new();
    pp.format(obj, &mut out, 0).unwrap();
    String::from_utf8(out).unwrap()
}

pub trait PPrint: Debug {
    fn dispatch() -> Option<Dispatch<Self>> {
        None
    }
}

pub struct PrettyPrinter {
    width: usize,
}

impl PrettyPrinter {
    // _format
    fn format<T: PPrint>(
        &self,
        object: &T,
        stream: &mut dyn io::Write,
        indent: usize,
    ) -> io::Result<()> {
        // The crux of the algorighm
        //
        // 1. Create a normal Debug view
        //     (This may be a recursive-safe one, but it doesnt have to be)
        // 2. If we have space to push it do that
        // 3. Otherwize, see if we have a specialization
        // 4. If we have a specialization, use that
        // 5. Otherwize, just add it and bite the bullet about size

        let repr = format!("{:?}", object);

        // What is allowance
        let max_width = self.width - indent;

        if repr.len() > max_width {
            if let Some(func) = T::dispatch() {
                return func(object, stream, self, indent);
            }
        }

        stream.write_all(repr.as_bytes())
    }
}
